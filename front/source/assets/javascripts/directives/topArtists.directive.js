(function () {
    'use strict';
    angular
        .module('app')
        .directive('topArtists', topArtists);

    function topArtists() {
        var directive = {
            restricts: 'E',
            templateUrl: 'views/topArtists.html',
            scope: {
                numTop: '='
            },
            controller: 'topArtistsCtrl',
            controllerAs: 'vm'
        };

        return directive;
    }
})();
