'use strict';

var gulp = require('gulp'),
    del = require('del'),
    plugins = require('gulp-load-plugins')({
        pattern: ['gulp-*', 'gulp.*'],
        replaceString: /^gulp(-|\.)/,
        lazy: false
    }),
    config = require('./config/config.json'),
    css = require('./config/styles.json');

gulp.task('styles', function() {
  return gulp.src(config.path.css)
    .pipe(plugins.sourcemaps.init())
    .pipe(plugins.sass({outputStyle: 'compressed'}).on('error', plugins.sass.logError))
    .pipe(plugins.autoprefixer({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    .pipe(plugins.rename('styles.css'))
    .pipe(plugins.combineMq({beautify: false}))
    .pipe(gulp.dest(config.path.build + 'css'))
    .pipe(plugins.cssnano())
    .pipe(plugins.sourcemaps.write('.'))
    .pipe(plugins.rename({ extname: '.min.css' }))
    .pipe(gulp.dest(config.path.build + 'css'));
});

gulp.task('css', function() {
  return gulp.src(css.path)
    .pipe(plugins.sourcemaps.init())
    .pipe(plugins.rename('style-plugins.css'))
    .pipe(plugins.rename({ extname: '.min.css' }))
    .pipe(gulp.dest(config.path.build + 'css'));
});
