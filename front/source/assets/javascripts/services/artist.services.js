(function () {
    'use strict';
    angular
        .module('app')
        .factory('artistService', ['$http', 'api', artistService]);

    function artistService($http, api) {
        var services;

        services = {
            getTopArtists: getTopArtists

        };

        return services;

        function success(response) {
            return response;
        }

        function fail(error) {
            return error;
        }

        function getTopArtists() {
            return $http({
                method: 'GET',
                url: api.url + '/2.0/?method=chart.gettopartists&api_key=' + api.key + '&format=json'
            })
                .success(success)
                .error(fail);
        }
    }
})();
