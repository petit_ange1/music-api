(function () {
    'use strict';
    angular
        .module('app')
        .directive('topTracks', topTracks);

    function topTracks() {
        var directive = {
            restricts: 'E',
            templateUrl: 'views/topTracks.html',
            controller: 'topTracksCtrl',
            controllerAs: 'vm',
            scope: {
                numTop: '='
            }
        };

        return directive;
    }
})();
