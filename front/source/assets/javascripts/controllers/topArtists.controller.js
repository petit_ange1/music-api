(function () {
    'use strict';

    angular
        .module('app')
        .controller('topArtistsCtrl', [
            '$scope',
            'artistService',
            'Notification',
            topArtistsCtrl
        ]);

    function topArtistsCtrl($scope, artistService, Notification) {
        var vm = this;
        vm.numTop = $scope.numTop || 50;
        artistService.getTopArtists().then(function (response) {
            vm.artists = response.data.artists.artist;
        }, function (error) {
            Notification.error(error.message);
        });
    }
})();
